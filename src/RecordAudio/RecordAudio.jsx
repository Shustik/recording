import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import s from './Record.module.css'



export const RecordAudio = () => {
  const [audio, setAudio] = useState(null)
  const [audioUrl, setAudioUrl] = useState(null)
  const [state, setState] = useState(true)
  useEffect(() => {
    if (!audio) {
      navigator.mediaDevices.getUserMedia({ audio: true })
        .then(stream => {
          const mediaRecorder = new MediaRecorder(stream)
          setAudio(mediaRecorder);
        })
    }
  }, [audio])

  if (!audio) {
    return null
  }
  const audioChunks = []
  audio.ondataavailable = (event) => {
    audioChunks.push(event.data)
  };
  audio.onstart = () => {
    setState(false)
  }
  audio.onstop = () => {
    const audioBlob = new Blob(audioChunks, {
      type: 'audio/wav'
    });
    setAudioUrl(URL.createObjectURL(audioBlob))
    setState(true)
  }

  return (
    <div className={s.container}>
      <div className={s.container__btn}>
        <div className={s.btn}>
        {state ? <button onClick={() => { audio.start() }} className={s.buttonRecord} >Recording</button>
          : <button onClick={() => { audio.stop() }} className={s.buttonStop} >Stop</button>}
      </div>
      <div>
        <NavLink to='/'>
          <img className={s.buttonHome} src="https://png.pngtree.com/png-vector/20190321/ourlarge/pngtree-vector-home-icon-png-image_856751.jpg" alt="" />
        </NavLink>
      </div>
      </div>
      <div>
        <audio src={audioUrl} className={s.audio} controls></audio>
      </div>
    </div>
  )
}






