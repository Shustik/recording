import './App.css';
import { ChoiceRecord } from './ChoiceRecord/ChoiceRecord';
import { Header } from './header/Header';
import { Route, Routes, BrowserRouter } from 'react-router-dom';
import { RecordAudio } from './RecordAudio/RecordAudio';
import { RecordVideo } from './RecordVideo/RecordVideo';


const App = () => {
  return (
    <div className='app'>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path='/' element={<ChoiceRecord />} />
          <Route path='/recordAudio' element={<RecordAudio />} />
          <Route path='/recordVideo' element={<RecordVideo />} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App;
