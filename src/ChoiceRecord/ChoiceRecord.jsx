import React from "react";
import { NavLink } from "react-router-dom";
import s from './ChoiceRecord.module.css'

export const ChoiceRecord = () => {

    return (
        <div className={s.container}>
            <div className={s.button__choice}>
                <NavLink to='/recordAudio'>
                    <img className={s.img} src="https://lumpics.ru/wp-content/uploads/2017/10/Kak-zapisat-audio-onlayn.png" alt="#" />
                </NavLink>
            </div>
            <div className={s.button__choice}>
                <NavLink to='/recordVideo'>
                    <img className={s.img} src="https://cdn-icons-png.flaticon.com/512/5225/5225451.png" alt="" />
                </NavLink>
            </div>


        </div>
    )
}